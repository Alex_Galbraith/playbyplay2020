﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleAnimator : MonoBehaviour
{
    public float PlaySpeed = 10;
    public Animation[] animations;
    private int animationFrame;
    private int animationIndex;
    private SpriteRenderer spriteRenderer;
    private Dictionary<string, int> nameToIndex = new Dictionary<string, int>();
    // Start is called before the first frame update
    void Start()
    {
        //sprite renderer is where we set the image each frame
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        StartCoroutine(AnimateRoutine());
    }

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    void Awake()
    {
        for (int i = 0; i < animations.Length; i++)
        {
            nameToIndex[animations[i].name] = i;
        }
    }
    public void SetAnimation(int index){
        animationIndex = index;
    }

    public void SetAnimation(string name){
        animationIndex = nameToIndex[name];
    }

    public void SetFrame(int index){
        animationFrame = index;
    }

    private IEnumerator AnimateRoutine(){
        while(true){
            yield return new WaitForSeconds(1/PlaySpeed);
            //advance animation by adding one, then looping around the length of the current animations sprite list
            //++ adds one, % is called the modulo operator which is a little complex
            animationFrame = ++animationFrame % animations[animationIndex].sprites.Length;
            //set the sprite to the current frame on the current animation
            spriteRenderer.sprite = animations[animationIndex].sprites[animationFrame];
        }
    }
    [System.Serializable]
    public struct Animation{
        public string name;
        public Sprite[] sprites;
    }
}
