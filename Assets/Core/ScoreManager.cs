﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    //This is an event. You're really going to want a good 
    //understanding of programming to understand these so I'll leave them
    //up to you to Google if you are interested
    public event Action<int, int> OnScoreChanged;
    private int score;
    //This is called a property. It is unique to C# and is a little complex
    //so if you are interested: https://www.w3schools.com/cs/cs_properties.asp
    public int Score{
        get{
            return score;
        }
        set{
            int old = score;
            score = value;
            OnScoreChanged?.Invoke(old, value);
        }
    }
}
