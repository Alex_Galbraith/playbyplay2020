﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    [SerializeField]
    private GameObject WinScreen;
    [SerializeField]
    private GameObject LoseScreen;
    public bool GameOver{
        get;
        private set;
    }

    public void WinGame(){
        WinScreen.SetActive(true);
        Time.timeScale = 0;
        GameOver = true;
    }

    public void LoseGame(){
        LoseScreen.SetActive(true);
        Time.timeScale = 0;
        GameOver = true;
    }

    public void RestartGame(){
        SceneManager.LoadScene(0);
        Time.timeScale = 1;
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
        if (GameOver && Input.anyKey){
            RestartGame();
        }
    }
}
