﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBehaviour : MonoBehaviour
{
    public Transform target;
    private float xOffset;
    // Start is called before the first frame update
    void Start()
    {
        xOffset = transform.position.x - target.position.x;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        transform.position = new Vector3 (target.position.x + xOffset, transform.position.y, transform.position.z);
    }
}
