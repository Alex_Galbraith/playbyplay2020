﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompletePlayerController : MonoBehaviour
{
    private Rigidbody2D rb2D;
    public float JumpForce;
    public ScoreManager scoreManager;
    public GameManager gameManager;
    public SimpleAnimator simpleAnimator;
    // Start is called before the first frame update
    void Start()
    {
        //could use GetComponent, but this is a little more reliable
        rb2D = GetComponentInChildren<Rigidbody2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //move forward always
        rb2D.AddForce(new Vector2(5,0));
        if(Input.anyKey){
            if(IsOnGround()){
                //jump up
                rb2D.velocity += Vector2.up * JumpForce;
                //jump a bit forward
                rb2D.velocity += Vector2.right * JumpForce * 0.2f;
            }
            //if we are pressing, fall slower and play jump animation
            rb2D.gravityScale = 1;
            simpleAnimator.SetAnimation("jump");

        }else{
            //if we arent pressing, fall faster and play walk animation
            rb2D.gravityScale = 4;
            simpleAnimator.SetAnimation("walk");
        }
    }

    /// <summary>
    /// Sent when another object enters a trigger collider attached to this
    /// object (2D physics only).
    /// </summary>
    /// <param name="other">The other Collider2D involved in this collision.</param>
    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag.Equals("Pickup")){
            Destroy(other.gameObject);
            scoreManager.Score += 100;
        }

        if(other.gameObject.tag.Equals("Win")){
            gameManager.WinGame();
        }

        if(other.gameObject.tag.Equals("Obstacle")){
            gameManager.LoseGame();
        }
    }

    /// <summary>
    /// Checks if the player is on the ground
    /// </summary>
    bool IsOnGround(){
        //Should do this in Start and save the value, GetComponent is expensive!
        //But for the sake of keeping things simple, I am doing it here
        Collider2D collider = GetComponentInChildren<Collider2D>();

        //cant be on the ground if we are travelling up
        if(!Mathf.Approximately(rb2D.velocity.y, 0))
            return false;
        RaycastHit2D[] hits = new RaycastHit2D[4];
        int hitC;
        //check if we hit a wall
        if ((hitC = collider.Cast(Vector3.down, hits, .1f)) > 0){
            for (int i = 0; i < hitC; i++)
            {
                if(hits[i].collider.gameObject.tag.Equals("Wall")){
                    return true;
                }
            }
        }
        return false;
    }

    
}
