// using System;
// using System.Collections.Generic;

// namespace PlayByPlay2020.Assets
// {
//     public class CustomDelegate<T>
//     {
//         public delegate T Delegate();
//         public Delegate func;

//         public CustomDelegate(Delegate d){
//             this.func = d;
//         }

//         public static implicit operator CustomDelegate<T>(Delegate d){
//             return new CustomDelegate<T>(d);
//         }

//     }

//     public class CustomEvent<T> {
//         private List<CustomDelegate<T>> events = new List<CustomDelegate<T>>();
//         public static CustomEvent<T> operator +(CustomEvent<T> c, CustomDelegate<T> d){
//             c.events.Add(d);
//             return c;
//         }
//     }

//     public class CustomEventTest{
//         public void Test(){
//             CustomEvent<int> eventTest = new CustomEvent<int>();
//             eventTest += (CustomDelegate<int>) SomeFunc;
//             eventTest += (CustomDelegate<int>) (()=>{return 5;});
//             eventTest += SomeFunc;
//         }

//         private int SomeFunc(){
//             return 5;
//         }
//     }
// }