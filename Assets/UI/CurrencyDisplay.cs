﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CurrencyDisplay : MonoBehaviour
{
    public Text CurrencyText;
    public Image CurrencyImage;
    public AnimationCurve ImageBumpEffectCurve = AnimationCurve.Constant(0,1,1);
    public ScoreManager scoreManager;


    // Start is called before the first frame update
    void Start()
    {
        //whenever the score is changed, we will get notified
        scoreManager.OnScoreChanged += HandleOnScoreChanged;
        SetCurrencyAmount(0, scoreManager.Score);
    }

    private void HandleOnScoreChanged(int from, int to)
    {
        SetCurrencyAmount(from,to);
    }

    public void SetCurrencyAmount(int from, int to){
        if(from == to){
            CurrencyText.text = $"{to}";
        }else{
            StartCoroutine(AnimateText(from, to, 0.5f));
            StartCoroutine(BumpImage(0.3f));
        }
    }

    private IEnumerator AnimateText (int from, int to, float time){
        float startTime = Time.unscaledTime;
        float elapsed = 0;
        while((elapsed = Time.unscaledTime - startTime) < time){
            float frac = elapsed/time;
            CurrencyText.text = $"{(int)(from + (to-from)*frac)}";
            yield return null;
        }
        CurrencyText.text = $"{(int)(to)}";
    }

    private IEnumerator BumpImage (float time){
        float startTime = Time.unscaledTime;
        float elapsed = 0;
        while((elapsed = Time.unscaledTime - startTime) < time){
            float frac = elapsed/time;
            float curved = ImageBumpEffectCurve.Evaluate(frac);
            CurrencyImage.transform.localScale = Vector3.one * curved;
            yield return null;
        }
        CurrencyImage.transform.localScale = Vector3.one;
    }
}
